import { Player } from './player';
export class Property {
    public name: string;
    public id: number;
    public posx?: number;
    public posy?: number;
    public color?: string;
    public owner: Player;
    public price?: number;
    public isSpecialProperty: boolean;
    public image?: string;

}