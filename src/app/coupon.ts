export class Coupon {
    private _callback: (...params) => any;
    private _description: string;

    constructor(description: string, callback: (...params) => any) {
        this._description = description;
        this._callback = callback;
    }

    public GetDescription() {
        return this._description;
    }

    public DoAction(...params) {
        this._callback(params);
    }
}