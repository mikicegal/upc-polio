import { Player } from './player';
import { Property } from './property';
import { Coupon } from './coupon';
import * as helper from './helper';


class Board {
    private readonly _BOARD_ID = 'board';
    private readonly _SPECIFICATIONS_TURN_DETAIL_ID = 'specs';
    private readonly _LIST_RENDER_PLAYERS_ID = 'listPlayers';
    private readonly _MATRIX_HEIGHT = 5;
    private readonly _MATRIX_WIDTH = 8;
    private readonly _NUM_MAX_DICES = 6;
    private readonly _INITIAL_CASH_PLAYERS = 150000;
    private readonly _PLAYER_SIZE_PIECE_PX = 12;
    private readonly _PLAYER_SIZE_MARGIN_PX = 5;
    private readonly _MAX_PLAYERS = 2;
    private readonly _PC_AS_PLAYER = false;
    private readonly _RIGHTS_OF_PASS = 5000;
    private readonly _GO_MONEY_RIGHT = 10000;
    private readonly _TAX_OVER_PROPERTY = 0.10;
    private readonly _JAIL_TAX = 10000;
    private readonly _MIN_PLAYER_AMOUNT = 3000;

    private _actualTurn = -1;
    private _properties: Array<Property> = [];
    private _bank: Player;
    private _players: Array<Player> = [];
    private _coupons: Array<Coupon> = [];
    private _boardWidthPX: number = 700;
    private _boardHeightPX: number = 500;
    private _propertyNormalWidth: number;
    private _propertyNormalHeight: number;
    private _propertyCornerHeight: number;
    private _propertyCornerWidth: number;
    private _propertyRotatedWidth: number;
    private _propertyRotatedHeight: number;

    private CalculatePropertiesSize() {
        this._propertyNormalWidth = this._boardWidthPX / (this._MATRIX_WIDTH);
        this._propertyNormalHeight = this._boardHeightPX / (this._MATRIX_HEIGHT);
        this._propertyCornerHeight = this._propertyNormalWidth;
        this._propertyCornerWidth = this._propertyNormalHeight;
        this._propertyRotatedWidth = (this._boardHeightPX - (2 * this._propertyNormalHeight)) / (this._MATRIX_HEIGHT - 2);
        this._propertyRotatedHeight = this._propertyNormalWidth;
    }

    private FindPlayerIndexPositionInCurrentProperty(currentPlayerProperty: Property): number {
        return currentPlayerProperty.id;
    }

    constructor(boardWidth?: number, boardHeight?: number) {
        this._boardWidthPX = boardWidth || this._boardWidthPX;
        this._boardHeightPX = boardHeight || this._boardHeightPX;
        this.CalculatePropertiesSize();
        this._bank = new Player("Bank", 100000, "#000", this._properties[0]);
        this._properties = [
            { id: 0, name: "Entrada", isSpecialProperty: true, owner: this._bank, image: 'img/go.png' },
            { id: 1, name: "Av Larco", isSpecialProperty: false, owner: this._bank, color: "#F44336", price: 13000 },
            { id: 2, name: "Av Arequipa", isSpecialProperty: false, owner: this._bank, color: "#F44336", price: 10000 },
            { id: 3, name: "Paseo de la Republica", isSpecialProperty: false, owner: this._bank, color: "#F44336", price: 90000 },
            { id: 4, name: "Cárcel", isSpecialProperty: true, owner: this._bank, image: 'img/only-visit.png' },
            { id: 5, name: "Jr. de la Unión", isSpecialProperty: false, owner: this._bank, color: "#3F51B5", price: 15000 },
            { id: 6, name: "Plz San Martín", isSpecialProperty: false, owner: this._bank, color: "#3F51B5", price: 14000 },
            { id: 7, name: "Centro Cívico", isSpecialProperty: false, owner: this._bank, color: "#3F51B5", price: 5000 },
            { id: 8, name: "Saque Cupón", isSpecialProperty: true, owner: this._bank, image: 'img/coupon.png' },
            { id: 9, name: "Av.Bolivar", isSpecialProperty: false, owner: this._bank, color: "#3F51B5", price: 17000 },
            { id: 10, name: "Pagar Impuestos", isSpecialProperty: true, owner: this._bank, image: 'img/tax.png' },
            { id: 11, name: "Descanso Gratis", isSpecialProperty: true, owner: this._bank, image: 'img/free-park.png' },
            { id: 12, name: "UNMSM", isSpecialProperty: false, owner: this._bank, color: "#8BC34A", price: 23000 },
            { id: 13, name: "Av. Venezuela", isSpecialProperty: false, owner: this._bank, color: "#8BC34A", price: 25000 },
            { id: 14, name: "Av. La Marina", isSpecialProperty: false, owner: this._bank, color: "#8BC34A", price: 17500 },
            { id: 15, name: "Vaya a la Cárcel", isSpecialProperty: true, owner: this._bank, image: 'img/jail.png' },
            { id: 16, name: "Saque Cupón", isSpecialProperty: true, owner: this._bank, image: 'img/coupon.png' },
            { id: 17, name: "Plz. San Miguel", isSpecialProperty: false, owner: this._bank, color: "#FFC107", price: 31000 },
            { id: 18, name: "UPC", isSpecialProperty: false, owner: this._bank, color: "#FFC107", price: 27000 },
            { id: 19, name: "Av. Primavera", isSpecialProperty: false, owner: this._bank, color: "#FFC107", price: 25000 },
            { id: 20, name: "Bembos Surco", isSpecialProperty: false, owner: this._bank, color: "#FFC107", price: 39000 },
            { id: 21, name: "Pagar Impuestos", isSpecialProperty: true, owner: this._bank, image: 'img/tax.png' }
        ]

        this._coupons.push(new Coupon('Avance hasta la entrada', () => {
            this._players[this._actualTurn].UpdateCurrentPropertyInBoard(this._properties[0]);
            this.UpdatePositionPlayers();
        }));

        this._coupons.push(new Coupon('Pague $2000 a los demás jugadores', () => {
            this._players.forEach((player, index) => {
                if (player.GetName() != this._players[this._actualTurn].GetName()) {
                    this._players[this._actualTurn].UpdateCash(2000, helper.SIGN.MINUS);
                    player.UpdateCash(2000, helper.SIGN.PLUS);
                }
            });
        }));

        this._coupons.push(new Coupon('Vaya directo a la cárcel, no pase por La entrada.', () => {
            this._players[this._actualTurn].UpdateCash(this._JAIL_TAX, helper.SIGN.MINUS);
            for (let property of this._properties) {
                if (property.name == 'Cárcel') {
                    this._players[this._actualTurn].UpdateCurrentPropertyInBoard(property);
                    break;
                }
            }
            this.UpdatePositionPlayers();
        }));

        this._coupons.push(new Coupon('Pague $1000 al dueño de la Av. Larco. si no tiene, se lo cargará al banco', () => {
            for (let property of this._properties) {
                if (property.name == 'Av Larco') {
                    this._players[this._actualTurn].UpdateCash(1000, helper.SIGN.MINUS);
                    this._players[this._actualTurn].GetCurrentPropertyInBoard().owner.UpdateCash(1000, helper.SIGN.PLUS);
                    break;
                }
            }
        }))

        this._coupons.push(new Coupon('Retroceda 5 espacios.', () => {
            this._players[this._actualTurn].UpdateCurrentPropertyInBoard(this._properties[this._players[this._actualTurn].GetCurrentPropertyInBoard().id - 5]);
            this.UpdatePositionPlayers();
        }));

        for (let i = 0; i < this._MAX_PLAYERS; i++) {
            let namePlayer = prompt(`Jugador ${i + 1} : Ingrese su nombre:`);
            let colorPlayer = prompt(`Jugador ${i + 1} : Ingrese un color (hexadecimal o en ingles):`);
            this._players.push(new Player(namePlayer, this._INITIAL_CASH_PLAYERS, colorPlayer, this._properties[0]));
            console.log('Jugador Creado: ', this._players[i]);
        }
        this.DrawBoard();
        this.DrawDice();
        this.DrawPlayers();
        this.DrawTurnSpecifications();
        this.DrawRendererButtons(false);
        $(document).on('click', '#playNextTurn', () => {
            this.PlayTurn();
        });

        $(document).on('click', '#buyProperty', (e) => {
            let priceBuyed = this._players[this._actualTurn].BuyProperty(this._players[this._actualTurn].GetCurrentPropertyInBoard());
            if (priceBuyed != -1) {
                alert(`Compraste la Propiedad ${this._players[this._actualTurn].GetCurrentPropertyInBoard().name} por ${this._players[this._actualTurn].GetCurrentPropertyInBoard().price}$`);
            } else {
                alert(`Lo sentimos, no cuentas con saldo suficiente como para comprar esta propiedad`);
            }

            this.UpdateTurnSpecifications(true, this._players[this._actualTurn]);
            e.preventDefault();
        });

        $(document).on('click', '#sellPropertyBank', (e) => {
            let priceSelled = this._players[this._actualTurn].SellProperty(this._players[this._actualTurn].GetCurrentPropertyInBoard(), this._bank);
            alert(`Se vendió la propiedad ${this._players[this._actualTurn].GetCurrentPropertyInBoard().name} a ${this._bank.GetName()} por un monto de  ${priceSelled}$ `);
            this.UpdateTurnSpecifications(true, this._players[this._actualTurn]);
            e.preventDefault();
        });

        $(document).on('click', '#sellPropertyPlayer', (e) => {
            this.ChoosePlayerToSell(this._players[this._actualTurn], (playerSelled) => {
                let priceSelled = this._players[this._actualTurn].SellProperty(this._players[this._actualTurn].GetCurrentPropertyInBoard(), playerSelled);
                if (priceSelled != -1) {
                    alert(`Se vendió la propiedad ${this._players[this._actualTurn].GetCurrentPropertyInBoard().name} a ${playerSelled.GetName()} por un monto de  ${priceSelled}$ `);

                } else {
                    alert(`Lo sentimos no se ha podido realizar la venta debido a que el jugador ${playerSelled.GetName} no cuenta con el dinero suficiente para dicha transacción`);
                }
                this.UpdateTurnSpecifications(true, this._players[this._actualTurn]);
            }, () => {
                alert('No se selecciono ningún comprador, no se ejecutará ninguna venta');
                this.UpdateTurnSpecifications(true, this._players[this._actualTurn]);
            });
            e.preventDefault();
        })
    }


    public ChoosePlayerToSell(actualplayer: Player, onSuccessChoose: (player: Player) => any, onCancelChoose: () => any) {
        let listPlayersToSell = "";
        this._players.forEach((player, index) => {
            if (player.GetName() != actualplayer.GetName()) {
                listPlayersToSell += `<div class='option-player-list' data-position='${index}'>${player.GetName()}</div>`;
            }
        });
        $('.modal-body').html(listPlayersToSell);
        $('.modal').fadeIn(500);
        let own = this;
        let onSuccessClick = function (e: JQueryEventObject) {
            onSuccessChoose(own._players[Number($(this).attr('data-position'))]);
            $('.modal').fadeOut(500);
            $(document).off('click', '.option-player-list', onSuccessClick);
            e.preventDefault()
        }
        $(document).on('click', '.option-player-list', onSuccessClick);

        let onCancelClick = function (e: JQueryEventObject) {
            $('.modal').fadeOut(500);
            onCancelChoose();
            $(document).off('click', '#modalCancel', onCancelChoose);
        }
        $(document).on('click', '#modalCancel', onCancelChoose);
    }

    public PlayTurn() {
        if (this._actualTurn == this._MAX_PLAYERS - 1) {
            this._actualTurn = 0;
        } else {
            this._actualTurn++;
        }

        let rollDice = this.PlayDice();
        if ((this.FindPlayerIndexPositionInCurrentProperty(this._players[this._actualTurn].GetCurrentPropertyInBoard()) + rollDice) > this._properties.length - 1) {
            alert(`Se le está pagando ${this._GO_MONEY_RIGHT}$ por pasar por la entrada`);
            this._players[this._actualTurn].UpdateCash(this._GO_MONEY_RIGHT, helper.SIGN.PLUS);
            let newPosition = rollDice - (this._properties.length - this.FindPlayerIndexPositionInCurrentProperty(this._players[this._actualTurn].GetCurrentPropertyInBoard()));
            this._players[this._actualTurn].UpdateCurrentPropertyInBoard(this._properties[newPosition]);
        } else {
            let newPosition = rollDice + this.FindPlayerIndexPositionInCurrentProperty(this._players[this._actualTurn].GetCurrentPropertyInBoard())
            this._players[this._actualTurn].UpdateCurrentPropertyInBoard(this._properties[newPosition]);
        }
        this.UpdatePositionPlayers();
        this.AnalizeTurn();
    }

    public AnalizeTurn() {
        if (this._players[this._actualTurn].GetCurrentPropertyInBoard().isSpecialProperty) {
            switch (this._players[this._actualTurn].GetCurrentPropertyInBoard().name) {
                case 'Saque Cupón': {
                    alert(`Ha caído en Saque Cupón`);
                    //TODO para sacar cupon
                    let randomCouponNumber = Math.floor(Math.random() * this._coupons.length);
                    alert(this._coupons[randomCouponNumber].GetDescription());
                    this._coupons[randomCouponNumber].DoAction();
                    break;
                }
                case 'Pagar Impuestos': {
                    alert(`Ha caído en Pagar impuestos, Usted tiene un total de ${this._players[this._actualTurn].GetTotalPriceOfOwnProperties()} en propiedades.\nSe le esta cobrando un total de: ${this._players[this._actualTurn].GetTotalPriceOfOwnProperties() * this._TAX_OVER_PROPERTY} $ (${this._TAX_OVER_PROPERTY * 100})% `);
                    let discount = this._players[this._actualTurn].GetTotalPriceOfOwnProperties() * this._TAX_OVER_PROPERTY;
                    this._players[this._actualTurn].UpdateCash(discount, helper.SIGN.MINUS);
                    break;
                }
                case 'Vaya a la Cárcel': {
                    alert(`Váyase directamente a la cárcel, No se le añadirá el cobro de la entrada y se le cobrará de multa ${this._JAIL_TAX}$`);
                    this._players[this._actualTurn].UpdateCash(this._JAIL_TAX, helper.SIGN.MINUS);
                    for (let property of this._properties) {
                        if (property.name == 'Cárcel') {
                            this._players[this._actualTurn].UpdateCurrentPropertyInBoard(property);
                            break;
                        }
                    }
                    this.UpdatePositionPlayers();
                    break;
                }
            }
        } else {
            if (this._players[this._actualTurn].GetCurrentPropertyInBoard().owner.GetName() != 'Bank' && this._players[this._actualTurn].GetName() != this._players[this._actualTurn].GetCurrentPropertyInBoard().owner.GetName()) {
                if (this._players[this._actualTurn].GetCash() < this._TAX_OVER_PROPERTY) {
                    alert('Usted ha sido descalificado del juego, ya no posee mas dinero para continuar');
                } else {
                    this._players[this._actualTurn].UpdateCash(this._RIGHTS_OF_PASS, helper.SIGN.MINUS);
                    this._players[this._actualTurn].GetCurrentPropertyInBoard().owner.UpdateCash(this._RIGHTS_OF_PASS, helper.SIGN.PLUS);
                    alert(`Usted ha caido en la propiedad de ${this._players[this._actualTurn].GetCurrentPropertyInBoard().owner.GetName()} se le está cobrando un saldo de ${this._RIGHTS_OF_PASS}$`);
                }

            }
        }
        this.UpdateTurnSpecifications(false, this._players[this._actualTurn]);
    }

    public DrawBoard() {
        $(`#${this._BOARD_ID}`).css({ 'margin': 0, 'padding': 0, 'position': 'relative', 'background-color': '#D5E8D4' });
        let templateBoard = ``;
        let angleToPlaceProperty = 0;
        let useWidthMatrixToCheckTurn = true;
        let printInverted = false;
        let numberPropertyToTurnAngle = '0';
        let bottomPosition = 0;
        let leftPosition = 0;
        for (let i in this._properties) {
            //orientación arriba -> derecha -> abajo ->inicio
            this._properties[i].posx = leftPosition;
            this._properties[i].posy = bottomPosition;
            if (i == numberPropertyToTurnAngle) {
                if (i != '0' && i != ((this._MATRIX_HEIGHT + this._MATRIX_WIDTH) - 2).toString()) {
                    angleToPlaceProperty += 90;
                }
                templateBoard += `<div style='height:${this._propertyNormalHeight}px; width:${this._propertyNormalWidth}px; position:absolute; bottom:${bottomPosition}px; left:${leftPosition}px; transform: rotate(${angleToPlaceProperty}deg); outline: 1px solid black;'><img src='${this._properties[i].image}'> ${this._properties[i].name}</div>`;
                if (i == '0' || i == ((this._MATRIX_HEIGHT + this._MATRIX_WIDTH) - 2).toString()) {
                    angleToPlaceProperty += 90;
                }
                if (angleToPlaceProperty > 180) {
                    printInverted = true;
                }
                if (useWidthMatrixToCheckTurn) {
                    numberPropertyToTurnAngle = (Number(numberPropertyToTurnAngle) + (this._MATRIX_HEIGHT - 1)).toString();
                    useWidthMatrixToCheckTurn = false;
                } else {
                    numberPropertyToTurnAngle = (Number(numberPropertyToTurnAngle) + (this._MATRIX_WIDTH - 1)).toString();
                    useWidthMatrixToCheckTurn = true;
                }
                if (useWidthMatrixToCheckTurn) {
                    if (!printInverted) {
                        leftPosition += this._propertyNormalWidth;
                    } else {
                        leftPosition -= this._propertyNormalWidth;
                    }

                } else {
                    if (!printInverted) {
                        bottomPosition += this._propertyNormalHeight;
                    } else {
                        bottomPosition -= this._propertyNormalHeight;
                    }

                }
            } else {
                if (useWidthMatrixToCheckTurn) {
                    if (this._properties[i].isSpecialProperty) {
                        templateBoard += `<div style='height:${this._propertyNormalHeight}px; width:${this._propertyNormalWidth}px; position:absolute; bottom:${bottomPosition}px; left:${leftPosition}px; transform: rotate(${angleToPlaceProperty}deg); outline: 1px solid black;'><img src='${this._properties[i].image}'>${this._properties[i].name}</div>`;
                    } else {
                        templateBoard += `<div style='height:${this._propertyNormalHeight}px; width:${this._propertyNormalWidth}px; position:absolute; bottom:${bottomPosition}px; left:${leftPosition}px; transform: rotate(${angleToPlaceProperty}deg);outline: 1px solid black;'><div style='display:block;background-color: ${this._properties[i].color}; height:${this._propertyNormalWidth / 4}px; border: 1px solid black;' ></div>${this._properties[i].name}<span style='position:absolute; bottom:2px; display:block; width: 100%; text-align:center'>${this._properties[i].price}$</span></div>`;
                    }
                    if (!printInverted) {
                        leftPosition += this._propertyNormalWidth;
                    } else {
                        leftPosition -= this._propertyNormalWidth;
                    }
                } else {
                    if (this._properties[i].isSpecialProperty) {
                        templateBoard += `<div style='height:${this._propertyRotatedHeight}px; width:${this._propertyRotatedWidth}px; position:absolute; bottom:${bottomPosition - ((this._propertyRotatedHeight - this._propertyRotatedWidth) / 2)}px; left:${leftPosition + ((this._propertyRotatedHeight - this._propertyRotatedWidth) / 2)}px; transform: rotate(${angleToPlaceProperty}deg); outline: 1px solid black;'><img src='${this._properties[i].image}'>${this._properties[i].name}</div>`;
                    } else {
                        templateBoard += `<div style='height:${this._propertyRotatedHeight}px; width:${this._propertyRotatedWidth}px; position:absolute; bottom:${bottomPosition - ((this._propertyRotatedHeight - this._propertyRotatedWidth) / 2)}px; left:${leftPosition + ((this._propertyRotatedHeight - this._propertyRotatedWidth) / 2)}px; transform: rotate(${angleToPlaceProperty}deg);outline: 1px solid black;'><div style='display:block;background-color: ${this._properties[i].color}; height:${this._propertyNormalWidth / 4}px; border: 1px solid black;' ></div>${this._properties[i].name}<span style='position:absolute; bottom:2px; display:block; width: 100%; text-align:center'>${this._properties[i].price}$</span></div>`;
                    }
                    if (!printInverted) {
                        bottomPosition += this._propertyRotatedWidth;
                    } else {
                        bottomPosition -= this._propertyRotatedWidth;
                    }
                }


            }
        }
        $(`#${this._BOARD_ID}`).html(templateBoard);
    }

    public DrawDice() {
        $(`#${this._BOARD_ID}`).append(`<img style='position:absolute; left:${this._boardWidthPX / 2}px; top:${this._boardHeightPX / 2}px; width:64px;' id='dice' src='img/dice-06.png' />`)
    }

    public UpdatePositionPlayers() {
        let countRestPlayers = this._MAX_PLAYERS;
        for (let i in this._players) {
            $(`#player${i}`).css({ 'bottom': `${(this._players[i].GetCurrentPropertyInBoard().posy + this._propertyNormalHeight / 2)}px`, 'left': `${(this._players[i].GetCurrentPropertyInBoard().posx + this._propertyNormalWidth / 2) - (countRestPlayers * this._PLAYER_SIZE_PIECE_PX)}px` });
            countRestPlayers--;
        }
    }

    public DrawCard(property: Property): string {
        let cardRender = '';
        if (property.isSpecialProperty) {
            cardRender = `<img src='${property.image}' style='width:50%; height:50%; object-fit:contain; margin-top:20%;' /><br/>${property.name}`;

        } else {
            cardRender = `<div style='height:15%; width:100%; background-color:${property.color}; border: 1px solid black; display:block; text-align:center;'></div>${property.name} <span style='position:absolute; width:100%; display:block; bottom:15px; text-align: center;'>${property.price}$</span>`;
        }
        $(`.card-container`).html(cardRender);
        return "";
    }

    public DrawRendererButtons(reRender: boolean, player?: Player | number) {
        let renderButtons = "<button type='button' id='playNextTurn' class='button'>Lanzar Dado</button>";
        player = player || -1;
        if (typeof player == 'number') {
            //ONLY PASS
        } else {
            if (player.GetCurrentPropertyInBoard().owner.GetName() == 'Bank' && !player.GetCurrentPropertyInBoard().isSpecialProperty && !reRender) {
                renderButtons += ` <button type='button' id='buyProperty' class='button'>Comprar Propiedad</button>`
            }
            if (player.GetCurrentPropertyInBoard().owner.GetName() == player.GetName() && !reRender) {
                renderButtons += ` <button type='button' id='sellPropertyBank' class='button'>Vender Propiedad al Banco</button> <button type='button' id='sellPropertyPlayer' class='button'>Vender Propiedad a otro jugador</button>`
            }
        }

        $('.rendered-buttons').html(renderButtons);
    }

    public UpdateTurnSpecifications(reRender: boolean, player: Player) {
        $('.card-container').fadeIn(500);
        $('.turno').html(`<h3 style='margin-bottom:15px; margin-top:15px;'>Turno: Jugador ${this._actualTurn + 1} <span style='width:20px; height:20px; border-radius:50%;display:inline-block; vertical-align:middle; background-color:${player.GetColor()};'></span>  ${player.GetName()}</h3`).css({ 'text-align': 'center' });
        this.DrawCard(player.GetCurrentPropertyInBoard());
        let infoPlayer = `<div><i class='fa fa-money'></i> <b>Dinero Actual: ${player.GetCash()}$</b> <br/><i class='fa fa-home'></i> Nombre Propiedad:${player.GetCurrentPropertyInBoard().name}`;
        if (!player.GetCurrentPropertyInBoard().isSpecialProperty) {
            infoPlayer += `</br><i class='fa fa-dollar'></i> Costo de Propiedad: ${player.GetCurrentPropertyInBoard().price}$<br/><i class='fa fa-user'></i> Dueño: <span style='width:12px; height:12px; border-radius:50%;display:inline-block; vertical-align:middle; background-color:${player.GetCurrentPropertyInBoard().owner.GetColor()};'></span> ${player.GetCurrentPropertyInBoard().owner.GetName()}`
        }
        infoPlayer += `</div>`;
        $('.info-player').html(infoPlayer);
        this.DrawRendererButtons(reRender, player);
    }

    public DrawTurnSpecifications() {
        let renderData = `<div class='turno'></div><div class='nombreJugador'></div><div class='card-container'></div><div class='info-player'></div><div class='rendered-buttons'></div>`;
        $(`#${this._SPECIFICATIONS_TURN_DETAIL_ID}`).html(renderData);
        $('.card-container').hide(0);
    }

    public DrawPlayers() {
        let countRestPlayers = this._MAX_PLAYERS;
        let renderedPlayers: string = "";
        for (let i in this._players) {
            renderedPlayers += `<div id='player${i}' style=' width:${this._PLAYER_SIZE_PIECE_PX}px; height:${this._PLAYER_SIZE_PIECE_PX}px; position: absolute; left: ${(this._players[i].GetCurrentPropertyInBoard().posx + this._propertyNormalWidth / 2) - (countRestPlayers * this._PLAYER_SIZE_PIECE_PX)}px; bottom: ${(this._players[i].GetCurrentPropertyInBoard().posy + this._propertyNormalHeight / 2)}px; border-radius: 50%; transition: all .5s; display:block; background-color: ${this._players[i].GetColor()};'>&nbsp;</div>`;
            countRestPlayers--;
        }
        $(`#${this._BOARD_ID}`).append(renderedPlayers);
    }

    public PlayDice(): number {
        let result = Math.floor(Math.random() * this._NUM_MAX_DICES) + 1;
        $('#dice').attr('src', `img/dice-0${result}.png`);
        return result
    }

    public RedrawAll() {
        this.DrawBoard();
        this.DrawDice();
        this.DrawPlayers();
    }


}

let game = new Board($('#board').outerWidth(), $('#board').outerHeight());


