import * as helper from './helper';
import { Property } from './property';
export class Player {
    private readonly _RESELL_BANK_TAX = 0.6;
    private readonly _RESELL_PLAYER_TAX = 0.8;
    private _name: string;
    private _cash: number;
    private _color: string;
    private _currentPropertyInBoard: Property;
    public ownedProperties: Array<Property>;


    private DeletePropertyFromOwnedProperties(property: Property): boolean {
        for (let i: number = 0; i < this.ownedProperties.length; i++) {
            if (property.id === this.ownedProperties[i].id) {
                this.ownedProperties.splice(i, 1);
                return true;
            }
        }
        return false;
    }

    constructor(name: string, initialCash: number, color: string, propertyInBoard: Property) {
        this._name = name;
        this._cash = initialCash;
        this._color = color;
        this._currentPropertyInBoard = propertyInBoard;
        this.ownedProperties = [];
    }

    public UpdateCash(amount: number, sign: helper.SIGN): void {
        if (sign == helper.SIGN.PLUS) {
            this._cash += amount;
        } else {
            this._cash -= amount;
        }
    }

    public GetCurrentPropertyInBoard(): Property {
        return this._currentPropertyInBoard;
    }

    public UpdateCurrentPropertyInBoard(upddatedProperty: Property): void {
        this._currentPropertyInBoard = upddatedProperty;
    }

    public GetName(): string {
        return this._name;
    }

    public GetColor(): string {
        return this._color;
    }

    public GetCash(): number {
        return this._cash;
    }

    public GetTotalPriceOfOwnProperties(): number {
        let total = 0;
        for (let property of this.ownedProperties) {
            total += property.price;
        }
        return total;
    }

    public BuyProperty(property: Property): number {
        if (property.owner.GetName() == 'Bank') {
            if (property.price <= this._cash) {
                this.UpdateCash(property.price, helper.SIGN.MINUS);
                property.owner = this;
                this.ownedProperties.push(property);
                return property.price;
            }
        }
        return -1;
    }

    public SellProperty(property: Property, player: Player): number {
        if (player.GetName() == 'Bank') {
            this.UpdateCash(property.price * this._RESELL_BANK_TAX, helper.SIGN.PLUS);
            this.DeletePropertyFromOwnedProperties(property);
            property.owner = player;
            return property.price * this._RESELL_BANK_TAX;
        } else {
            if (player.GetCash() >= property.price * this._RESELL_PLAYER_TAX) {
                this.UpdateCash(property.price * this._RESELL_PLAYER_TAX, helper.SIGN.PLUS);
                player.UpdateCash(property.price * this._RESELL_PLAYER_TAX, helper.SIGN.MINUS);
                this.DeletePropertyFromOwnedProperties(property);
                player.ownedProperties.push(property);
                property.owner = player;
                return property.price * this._RESELL_PLAYER_TAX;
            } else {
                return -1;
            }
        }

    }
}