import * as express from 'express';
import * as compression from 'compression';
var app = express();
app.use(compression());
app.use(express.static(__dirname + '/../'));
app.get('/', function (request, response) {
    response.sendFile(__dirname + '/../index.html');
});
app.listen(3000, function () { });
