"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var compression = require("compression");
var app = express();
app.use(compression());
app.use(express.static(__dirname + '/../'));
app.get('/', function (request, response) {
    response.sendFile(__dirname + '/../index.html');
});
app.listen(3000, function () { });
//# sourceMappingURL=index.js.map