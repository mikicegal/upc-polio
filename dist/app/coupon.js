"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Coupon = (function () {
    function Coupon(description, callback) {
        this._description = description;
        this._callback = callback;
    }
    Coupon.prototype.GetDescription = function () {
        return this._description;
    };
    Coupon.prototype.DoAction = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        this._callback(params);
    };
    return Coupon;
}());
exports.Coupon = Coupon;
//# sourceMappingURL=coupon.js.map