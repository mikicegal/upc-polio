"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var helper = require("./helper");
var Player = (function () {
    function Player(name, initialCash, color, propertyInBoard) {
        this._RESELL_BANK_TAX = 0.6;
        this._RESELL_PLAYER_TAX = 0.8;
        this._name = name;
        this._cash = initialCash;
        this._color = color;
        this._currentPropertyInBoard = propertyInBoard;
        this.ownedProperties = [];
    }
    Player.prototype.DeletePropertyFromOwnedProperties = function (property) {
        for (var i = 0; i < this.ownedProperties.length; i++) {
            if (property.id === this.ownedProperties[i].id) {
                this.ownedProperties.splice(i, 1);
                return true;
            }
        }
        return false;
    };
    Player.prototype.UpdateCash = function (amount, sign) {
        if (sign == helper.SIGN.PLUS) {
            this._cash += amount;
        }
        else {
            this._cash -= amount;
        }
    };
    Player.prototype.GetCurrentPropertyInBoard = function () {
        return this._currentPropertyInBoard;
    };
    Player.prototype.UpdateCurrentPropertyInBoard = function (upddatedProperty) {
        this._currentPropertyInBoard = upddatedProperty;
    };
    Player.prototype.GetName = function () {
        return this._name;
    };
    Player.prototype.GetColor = function () {
        return this._color;
    };
    Player.prototype.GetCash = function () {
        return this._cash;
    };
    Player.prototype.GetTotalPriceOfOwnProperties = function () {
        var total = 0;
        for (var _i = 0, _a = this.ownedProperties; _i < _a.length; _i++) {
            var property = _a[_i];
            total += property.price;
        }
        return total;
    };
    Player.prototype.BuyProperty = function (property) {
        if (property.owner.GetName() == 'Bank') {
            if (property.price <= this._cash) {
                this.UpdateCash(property.price, helper.SIGN.MINUS);
                property.owner = this;
                this.ownedProperties.push(property);
                return property.price;
            }
        }
        return -1;
    };
    Player.prototype.SellProperty = function (property, player) {
        if (player.GetName() == 'Bank') {
            this.UpdateCash(property.price * this._RESELL_BANK_TAX, helper.SIGN.PLUS);
            this.DeletePropertyFromOwnedProperties(property);
            property.owner = player;
            return property.price * this._RESELL_BANK_TAX;
        }
        else {
            if (player.GetCash() >= property.price * this._RESELL_PLAYER_TAX) {
                this.UpdateCash(property.price * this._RESELL_PLAYER_TAX, helper.SIGN.PLUS);
                player.UpdateCash(property.price * this._RESELL_PLAYER_TAX, helper.SIGN.MINUS);
                this.DeletePropertyFromOwnedProperties(property);
                player.ownedProperties.push(property);
                property.owner = player;
                return property.price * this._RESELL_PLAYER_TAX;
            }
            else {
                return -1;
            }
        }
    };
    return Player;
}());
exports.Player = Player;
//# sourceMappingURL=player.js.map