"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var player_1 = require("./player");
var coupon_1 = require("./coupon");
var helper = require("./helper");
var Board = (function () {
    function Board(boardWidth, boardHeight) {
        var _this = this;
        this._BOARD_ID = 'board';
        this._SPECIFICATIONS_TURN_DETAIL_ID = 'specs';
        this._LIST_RENDER_PLAYERS_ID = 'listPlayers';
        this._MATRIX_HEIGHT = 5;
        this._MATRIX_WIDTH = 8;
        this._NUM_MAX_DICES = 6;
        this._INITIAL_CASH_PLAYERS = 150000;
        this._PLAYER_SIZE_PIECE_PX = 12;
        this._PLAYER_SIZE_MARGIN_PX = 5;
        this._MAX_PLAYERS = 2;
        this._PC_AS_PLAYER = false;
        this._RIGHTS_OF_PASS = 5000;
        this._GO_MONEY_RIGHT = 10000;
        this._TAX_OVER_PROPERTY = 0.10;
        this._JAIL_TAX = 10000;
        this._MIN_PLAYER_AMOUNT = 3000;
        this._actualTurn = -1;
        this._properties = [];
        this._players = [];
        this._coupons = [];
        this._boardWidthPX = 700;
        this._boardHeightPX = 500;
        this._boardWidthPX = boardWidth || this._boardWidthPX;
        this._boardHeightPX = boardHeight || this._boardHeightPX;
        this.CalculatePropertiesSize();
        this._bank = new player_1.Player("Bank", 100000, "#000", this._properties[0]);
        this._properties = [
            { id: 0, name: "Entrada", isSpecialProperty: true, owner: this._bank, image: 'img/go.png' },
            { id: 1, name: "Av Larco", isSpecialProperty: false, owner: this._bank, color: "#F44336", price: 13000 },
            { id: 2, name: "Av Arequipa", isSpecialProperty: false, owner: this._bank, color: "#F44336", price: 10000 },
            { id: 3, name: "Paseo de la Republica", isSpecialProperty: false, owner: this._bank, color: "#F44336", price: 90000 },
            { id: 4, name: "Cárcel", isSpecialProperty: true, owner: this._bank, image: 'img/only-visit.png' },
            { id: 5, name: "Jr. de la Unión", isSpecialProperty: false, owner: this._bank, color: "#3F51B5", price: 15000 },
            { id: 6, name: "Plz San Martín", isSpecialProperty: false, owner: this._bank, color: "#3F51B5", price: 14000 },
            { id: 7, name: "Centro Cívico", isSpecialProperty: false, owner: this._bank, color: "#3F51B5", price: 5000 },
            { id: 8, name: "Saque Cupón", isSpecialProperty: true, owner: this._bank, image: 'img/coupon.png' },
            { id: 9, name: "Av.Bolivar", isSpecialProperty: false, owner: this._bank, color: "#3F51B5", price: 17000 },
            { id: 10, name: "Pagar Impuestos", isSpecialProperty: true, owner: this._bank, image: 'img/tax.png' },
            { id: 11, name: "Descanso Gratis", isSpecialProperty: true, owner: this._bank, image: 'img/free-park.png' },
            { id: 12, name: "UNMSM", isSpecialProperty: false, owner: this._bank, color: "#8BC34A", price: 23000 },
            { id: 13, name: "Av. Venezuela", isSpecialProperty: false, owner: this._bank, color: "#8BC34A", price: 25000 },
            { id: 14, name: "Av. La Marina", isSpecialProperty: false, owner: this._bank, color: "#8BC34A", price: 17500 },
            { id: 15, name: "Vaya a la Cárcel", isSpecialProperty: true, owner: this._bank, image: 'img/jail.png' },
            { id: 16, name: "Saque Cupón", isSpecialProperty: true, owner: this._bank, image: 'img/coupon.png' },
            { id: 17, name: "Plz. San Miguel", isSpecialProperty: false, owner: this._bank, color: "#FFC107", price: 31000 },
            { id: 18, name: "UPC", isSpecialProperty: false, owner: this._bank, color: "#FFC107", price: 27000 },
            { id: 19, name: "Av. Primavera", isSpecialProperty: false, owner: this._bank, color: "#FFC107", price: 25000 },
            { id: 20, name: "Bembos Surco", isSpecialProperty: false, owner: this._bank, color: "#FFC107", price: 39000 },
            { id: 21, name: "Pagar Impuestos", isSpecialProperty: true, owner: this._bank, image: 'img/tax.png' }
        ];
        this._coupons.push(new coupon_1.Coupon('Avance hasta la entrada', function () {
            _this._players[_this._actualTurn].UpdateCurrentPropertyInBoard(_this._properties[0]);
            _this.UpdatePositionPlayers();
        }));
        this._coupons.push(new coupon_1.Coupon('Pague $2000 a los demás jugadores', function () {
            _this._players.forEach(function (player, index) {
                if (player.GetName() != _this._players[_this._actualTurn].GetName()) {
                    _this._players[_this._actualTurn].UpdateCash(2000, helper.SIGN.MINUS);
                    player.UpdateCash(2000, helper.SIGN.PLUS);
                }
            });
        }));
        this._coupons.push(new coupon_1.Coupon('Vaya directo a la cárcel, no pase por La entrada.', function () {
            _this._players[_this._actualTurn].UpdateCash(_this._JAIL_TAX, helper.SIGN.MINUS);
            for (var _i = 0, _a = _this._properties; _i < _a.length; _i++) {
                var property = _a[_i];
                if (property.name == 'Cárcel') {
                    _this._players[_this._actualTurn].UpdateCurrentPropertyInBoard(property);
                    break;
                }
            }
            _this.UpdatePositionPlayers();
        }));
        this._coupons.push(new coupon_1.Coupon('Pague $1000 al dueño de la Av. Larco. si no tiene, se lo cargará al banco', function () {
            for (var _i = 0, _a = _this._properties; _i < _a.length; _i++) {
                var property = _a[_i];
                if (property.name == 'Av Larco') {
                    _this._players[_this._actualTurn].UpdateCash(1000, helper.SIGN.MINUS);
                    _this._players[_this._actualTurn].GetCurrentPropertyInBoard().owner.UpdateCash(1000, helper.SIGN.PLUS);
                    break;
                }
            }
        }));
        this._coupons.push(new coupon_1.Coupon('Retroceda 5 espacios.', function () {
            _this._players[_this._actualTurn].UpdateCurrentPropertyInBoard(_this._properties[_this._players[_this._actualTurn].GetCurrentPropertyInBoard().id - 5]);
            _this.UpdatePositionPlayers();
        }));
        for (var i = 0; i < this._MAX_PLAYERS; i++) {
            var namePlayer = prompt("Jugador " + (i + 1) + " : Ingrese su nombre:");
            var colorPlayer = prompt("Jugador " + (i + 1) + " : Ingrese un color (hexadecimal o en ingles):");
            this._players.push(new player_1.Player(namePlayer, this._INITIAL_CASH_PLAYERS, colorPlayer, this._properties[0]));
            console.log('Jugador Creado: ', this._players[i]);
        }
        this.DrawBoard();
        this.DrawDice();
        this.DrawPlayers();
        this.DrawTurnSpecifications();
        this.DrawRendererButtons(false);
        $(document).on('click', '#playNextTurn', function () {
            _this.PlayTurn();
        });
        $(document).on('click', '#buyProperty', function (e) {
            var priceBuyed = _this._players[_this._actualTurn].BuyProperty(_this._players[_this._actualTurn].GetCurrentPropertyInBoard());
            if (priceBuyed != -1) {
                alert("Compraste la Propiedad " + _this._players[_this._actualTurn].GetCurrentPropertyInBoard().name + " por " + _this._players[_this._actualTurn].GetCurrentPropertyInBoard().price + "$");
            }
            else {
                alert("Lo sentimos, no cuentas con saldo suficiente como para comprar esta propiedad");
            }
            _this.UpdateTurnSpecifications(true, _this._players[_this._actualTurn]);
            e.preventDefault();
        });
        $(document).on('click', '#sellPropertyBank', function (e) {
            var priceSelled = _this._players[_this._actualTurn].SellProperty(_this._players[_this._actualTurn].GetCurrentPropertyInBoard(), _this._bank);
            alert("Se vendi\u00F3 la propiedad " + _this._players[_this._actualTurn].GetCurrentPropertyInBoard().name + " a " + _this._bank.GetName() + " por un monto de  " + priceSelled + "$ ");
            _this.UpdateTurnSpecifications(true, _this._players[_this._actualTurn]);
            e.preventDefault();
        });
        $(document).on('click', '#sellPropertyPlayer', function (e) {
            _this.ChoosePlayerToSell(_this._players[_this._actualTurn], function (playerSelled) {
                var priceSelled = _this._players[_this._actualTurn].SellProperty(_this._players[_this._actualTurn].GetCurrentPropertyInBoard(), playerSelled);
                if (priceSelled != -1) {
                    alert("Se vendi\u00F3 la propiedad " + _this._players[_this._actualTurn].GetCurrentPropertyInBoard().name + " a " + playerSelled.GetName() + " por un monto de  " + priceSelled + "$ ");
                }
                else {
                    alert("Lo sentimos no se ha podido realizar la venta debido a que el jugador " + playerSelled.GetName + " no cuenta con el dinero suficiente para dicha transacci\u00F3n");
                }
                _this.UpdateTurnSpecifications(true, _this._players[_this._actualTurn]);
            }, function () {
                alert('No se selecciono ningún comprador, no se ejecutará ninguna venta');
                _this.UpdateTurnSpecifications(true, _this._players[_this._actualTurn]);
            });
            e.preventDefault();
        });
    }
    Board.prototype.CalculatePropertiesSize = function () {
        this._propertyNormalWidth = this._boardWidthPX / (this._MATRIX_WIDTH);
        this._propertyNormalHeight = this._boardHeightPX / (this._MATRIX_HEIGHT);
        this._propertyCornerHeight = this._propertyNormalWidth;
        this._propertyCornerWidth = this._propertyNormalHeight;
        this._propertyRotatedWidth = (this._boardHeightPX - (2 * this._propertyNormalHeight)) / (this._MATRIX_HEIGHT - 2);
        this._propertyRotatedHeight = this._propertyNormalWidth;
    };
    Board.prototype.FindPlayerIndexPositionInCurrentProperty = function (currentPlayerProperty) {
        return currentPlayerProperty.id;
    };
    Board.prototype.ChoosePlayerToSell = function (actualplayer, onSuccessChoose, onCancelChoose) {
        var listPlayersToSell = "";
        this._players.forEach(function (player, index) {
            if (player.GetName() != actualplayer.GetName()) {
                listPlayersToSell += "<div class='option-player-list' data-position='" + index + "'>" + player.GetName() + "</div>";
            }
        });
        $('.modal-body').html(listPlayersToSell);
        $('.modal').fadeIn(500);
        var own = this;
        var onSuccessClick = function (e) {
            onSuccessChoose(own._players[Number($(this).attr('data-position'))]);
            $('.modal').fadeOut(500);
            $(document).off('click', '.option-player-list', onSuccessClick);
            e.preventDefault();
        };
        $(document).on('click', '.option-player-list', onSuccessClick);
        var onCancelClick = function (e) {
            $('.modal').fadeOut(500);
            onCancelChoose();
            $(document).off('click', '#modalCancel', onCancelChoose);
        };
        $(document).on('click', '#modalCancel', onCancelChoose);
    };
    Board.prototype.PlayTurn = function () {
        if (this._actualTurn == this._MAX_PLAYERS - 1) {
            this._actualTurn = 0;
        }
        else {
            this._actualTurn++;
        }
        var rollDice = this.PlayDice();
        if ((this.FindPlayerIndexPositionInCurrentProperty(this._players[this._actualTurn].GetCurrentPropertyInBoard()) + rollDice) > this._properties.length - 1) {
            alert("Se le est\u00E1 pagando " + this._GO_MONEY_RIGHT + "$ por pasar por la entrada");
            this._players[this._actualTurn].UpdateCash(this._GO_MONEY_RIGHT, helper.SIGN.PLUS);
            var newPosition = rollDice - (this._properties.length - this.FindPlayerIndexPositionInCurrentProperty(this._players[this._actualTurn].GetCurrentPropertyInBoard()));
            this._players[this._actualTurn].UpdateCurrentPropertyInBoard(this._properties[newPosition]);
        }
        else {
            var newPosition = rollDice + this.FindPlayerIndexPositionInCurrentProperty(this._players[this._actualTurn].GetCurrentPropertyInBoard());
            this._players[this._actualTurn].UpdateCurrentPropertyInBoard(this._properties[newPosition]);
        }
        this.UpdatePositionPlayers();
        this.AnalizeTurn();
    };
    Board.prototype.AnalizeTurn = function () {
        if (this._players[this._actualTurn].GetCurrentPropertyInBoard().isSpecialProperty) {
            switch (this._players[this._actualTurn].GetCurrentPropertyInBoard().name) {
                case 'Saque Cupón': {
                    alert("Ha ca\u00EDdo en Saque Cup\u00F3n");
                    var randomCouponNumber = Math.floor(Math.random() * this._coupons.length);
                    alert(this._coupons[randomCouponNumber].GetDescription());
                    this._coupons[randomCouponNumber].DoAction();
                    break;
                }
                case 'Pagar Impuestos': {
                    alert("Ha ca\u00EDdo en Pagar impuestos, Usted tiene un total de " + this._players[this._actualTurn].GetTotalPriceOfOwnProperties() + " en propiedades.\nSe le esta cobrando un total de: " + this._players[this._actualTurn].GetTotalPriceOfOwnProperties() * this._TAX_OVER_PROPERTY + " $ (" + this._TAX_OVER_PROPERTY * 100 + ")% ");
                    var discount = this._players[this._actualTurn].GetTotalPriceOfOwnProperties() * this._TAX_OVER_PROPERTY;
                    this._players[this._actualTurn].UpdateCash(discount, helper.SIGN.MINUS);
                    break;
                }
                case 'Vaya a la Cárcel': {
                    alert("V\u00E1yase directamente a la c\u00E1rcel, No se le a\u00F1adir\u00E1 el cobro de la entrada y se le cobrar\u00E1 de multa " + this._JAIL_TAX + "$");
                    this._players[this._actualTurn].UpdateCash(this._JAIL_TAX, helper.SIGN.MINUS);
                    for (var _i = 0, _a = this._properties; _i < _a.length; _i++) {
                        var property = _a[_i];
                        if (property.name == 'Cárcel') {
                            this._players[this._actualTurn].UpdateCurrentPropertyInBoard(property);
                            break;
                        }
                    }
                    this.UpdatePositionPlayers();
                    break;
                }
            }
        }
        else {
            if (this._players[this._actualTurn].GetCurrentPropertyInBoard().owner.GetName() != 'Bank' && this._players[this._actualTurn].GetName() != this._players[this._actualTurn].GetCurrentPropertyInBoard().owner.GetName()) {
                if (this._players[this._actualTurn].GetCash() < this._TAX_OVER_PROPERTY) {
                    alert('Usted ha sido descalificado del juego, ya no posee mas dinero para continuar');
                }
                else {
                    this._players[this._actualTurn].UpdateCash(this._RIGHTS_OF_PASS, helper.SIGN.MINUS);
                    this._players[this._actualTurn].GetCurrentPropertyInBoard().owner.UpdateCash(this._RIGHTS_OF_PASS, helper.SIGN.PLUS);
                    alert("Usted ha caido en la propiedad de " + this._players[this._actualTurn].GetCurrentPropertyInBoard().owner.GetName() + " se le est\u00E1 cobrando un saldo de " + this._RIGHTS_OF_PASS + "$");
                }
            }
        }
        this.UpdateTurnSpecifications(false, this._players[this._actualTurn]);
    };
    Board.prototype.DrawBoard = function () {
        $("#" + this._BOARD_ID).css({ 'margin': 0, 'padding': 0, 'position': 'relative', 'background-color': '#D5E8D4' });
        var templateBoard = "";
        var angleToPlaceProperty = 0;
        var useWidthMatrixToCheckTurn = true;
        var printInverted = false;
        var numberPropertyToTurnAngle = '0';
        var bottomPosition = 0;
        var leftPosition = 0;
        for (var i in this._properties) {
            this._properties[i].posx = leftPosition;
            this._properties[i].posy = bottomPosition;
            if (i == numberPropertyToTurnAngle) {
                if (i != '0' && i != ((this._MATRIX_HEIGHT + this._MATRIX_WIDTH) - 2).toString()) {
                    angleToPlaceProperty += 90;
                }
                templateBoard += "<div style='height:" + this._propertyNormalHeight + "px; width:" + this._propertyNormalWidth + "px; position:absolute; bottom:" + bottomPosition + "px; left:" + leftPosition + "px; transform: rotate(" + angleToPlaceProperty + "deg); outline: 1px solid black;'><img src='" + this._properties[i].image + "'> " + this._properties[i].name + "</div>";
                if (i == '0' || i == ((this._MATRIX_HEIGHT + this._MATRIX_WIDTH) - 2).toString()) {
                    angleToPlaceProperty += 90;
                }
                if (angleToPlaceProperty > 180) {
                    printInverted = true;
                }
                if (useWidthMatrixToCheckTurn) {
                    numberPropertyToTurnAngle = (Number(numberPropertyToTurnAngle) + (this._MATRIX_HEIGHT - 1)).toString();
                    useWidthMatrixToCheckTurn = false;
                }
                else {
                    numberPropertyToTurnAngle = (Number(numberPropertyToTurnAngle) + (this._MATRIX_WIDTH - 1)).toString();
                    useWidthMatrixToCheckTurn = true;
                }
                if (useWidthMatrixToCheckTurn) {
                    if (!printInverted) {
                        leftPosition += this._propertyNormalWidth;
                    }
                    else {
                        leftPosition -= this._propertyNormalWidth;
                    }
                }
                else {
                    if (!printInverted) {
                        bottomPosition += this._propertyNormalHeight;
                    }
                    else {
                        bottomPosition -= this._propertyNormalHeight;
                    }
                }
            }
            else {
                if (useWidthMatrixToCheckTurn) {
                    if (this._properties[i].isSpecialProperty) {
                        templateBoard += "<div style='height:" + this._propertyNormalHeight + "px; width:" + this._propertyNormalWidth + "px; position:absolute; bottom:" + bottomPosition + "px; left:" + leftPosition + "px; transform: rotate(" + angleToPlaceProperty + "deg); outline: 1px solid black;'><img src='" + this._properties[i].image + "'>" + this._properties[i].name + "</div>";
                    }
                    else {
                        templateBoard += "<div style='height:" + this._propertyNormalHeight + "px; width:" + this._propertyNormalWidth + "px; position:absolute; bottom:" + bottomPosition + "px; left:" + leftPosition + "px; transform: rotate(" + angleToPlaceProperty + "deg);outline: 1px solid black;'><div style='display:block;background-color: " + this._properties[i].color + "; height:" + this._propertyNormalWidth / 4 + "px; border: 1px solid black;' ></div>" + this._properties[i].name + "<span style='position:absolute; bottom:2px; display:block; width: 100%; text-align:center'>" + this._properties[i].price + "$</span></div>";
                    }
                    if (!printInverted) {
                        leftPosition += this._propertyNormalWidth;
                    }
                    else {
                        leftPosition -= this._propertyNormalWidth;
                    }
                }
                else {
                    if (this._properties[i].isSpecialProperty) {
                        templateBoard += "<div style='height:" + this._propertyRotatedHeight + "px; width:" + this._propertyRotatedWidth + "px; position:absolute; bottom:" + (bottomPosition - ((this._propertyRotatedHeight - this._propertyRotatedWidth) / 2)) + "px; left:" + (leftPosition + ((this._propertyRotatedHeight - this._propertyRotatedWidth) / 2)) + "px; transform: rotate(" + angleToPlaceProperty + "deg); outline: 1px solid black;'><img src='" + this._properties[i].image + "'>" + this._properties[i].name + "</div>";
                    }
                    else {
                        templateBoard += "<div style='height:" + this._propertyRotatedHeight + "px; width:" + this._propertyRotatedWidth + "px; position:absolute; bottom:" + (bottomPosition - ((this._propertyRotatedHeight - this._propertyRotatedWidth) / 2)) + "px; left:" + (leftPosition + ((this._propertyRotatedHeight - this._propertyRotatedWidth) / 2)) + "px; transform: rotate(" + angleToPlaceProperty + "deg);outline: 1px solid black;'><div style='display:block;background-color: " + this._properties[i].color + "; height:" + this._propertyNormalWidth / 4 + "px; border: 1px solid black;' ></div>" + this._properties[i].name + "<span style='position:absolute; bottom:2px; display:block; width: 100%; text-align:center'>" + this._properties[i].price + "$</span></div>";
                    }
                    if (!printInverted) {
                        bottomPosition += this._propertyRotatedWidth;
                    }
                    else {
                        bottomPosition -= this._propertyRotatedWidth;
                    }
                }
            }
        }
        $("#" + this._BOARD_ID).html(templateBoard);
    };
    Board.prototype.DrawDice = function () {
        $("#" + this._BOARD_ID).append("<img style='position:absolute; left:" + this._boardWidthPX / 2 + "px; top:" + this._boardHeightPX / 2 + "px; width:64px;' id='dice' src='img/dice-06.png' />");
    };
    Board.prototype.UpdatePositionPlayers = function () {
        var countRestPlayers = this._MAX_PLAYERS;
        for (var i in this._players) {
            $("#player" + i).css({ 'bottom': (this._players[i].GetCurrentPropertyInBoard().posy + this._propertyNormalHeight / 2) + "px", 'left': (this._players[i].GetCurrentPropertyInBoard().posx + this._propertyNormalWidth / 2) - (countRestPlayers * this._PLAYER_SIZE_PIECE_PX) + "px" });
            countRestPlayers--;
        }
    };
    Board.prototype.DrawCard = function (property) {
        var cardRender = '';
        if (property.isSpecialProperty) {
            cardRender = "<img src='" + property.image + "' style='width:50%; height:50%; object-fit:contain; margin-top:20%;' /><br/>" + property.name;
        }
        else {
            cardRender = "<div style='height:15%; width:100%; background-color:" + property.color + "; border: 1px solid black; display:block; text-align:center;'></div>" + property.name + " <span style='position:absolute; width:100%; display:block; bottom:15px; text-align: center;'>" + property.price + "$</span>";
        }
        $(".card-container").html(cardRender);
        return "";
    };
    Board.prototype.DrawRendererButtons = function (reRender, player) {
        var renderButtons = "<button type='button' id='playNextTurn' class='button'>Lanzar Dado</button>";
        player = player || -1;
        if (typeof player == 'number') {
        }
        else {
            if (player.GetCurrentPropertyInBoard().owner.GetName() == 'Bank' && !player.GetCurrentPropertyInBoard().isSpecialProperty && !reRender) {
                renderButtons += " <button type='button' id='buyProperty' class='button'>Comprar Propiedad</button>";
            }
            if (player.GetCurrentPropertyInBoard().owner.GetName() == player.GetName() && !reRender) {
                renderButtons += " <button type='button' id='sellPropertyBank' class='button'>Vender Propiedad al Banco</button> <button type='button' id='sellPropertyPlayer' class='button'>Vender Propiedad a otro jugador</button>";
            }
        }
        $('.rendered-buttons').html(renderButtons);
    };
    Board.prototype.UpdateTurnSpecifications = function (reRender, player) {
        $('.card-container').fadeIn(500);
        $('.turno').html("<h3 style='margin-bottom:15px; margin-top:15px;'>Turno: Jugador " + (this._actualTurn + 1) + " <span style='width:20px; height:20px; border-radius:50%;display:inline-block; vertical-align:middle; background-color:" + player.GetColor() + ";'></span>  " + player.GetName() + "</h3").css({ 'text-align': 'center' });
        this.DrawCard(player.GetCurrentPropertyInBoard());
        var infoPlayer = "<div><i class='fa fa-money'></i> <b>Dinero Actual: " + player.GetCash() + "$</b> <br/><i class='fa fa-home'></i> Nombre Propiedad:" + player.GetCurrentPropertyInBoard().name;
        if (!player.GetCurrentPropertyInBoard().isSpecialProperty) {
            infoPlayer += "</br><i class='fa fa-dollar'></i> Costo de Propiedad: " + player.GetCurrentPropertyInBoard().price + "$<br/><i class='fa fa-user'></i> Due\u00F1o: <span style='width:12px; height:12px; border-radius:50%;display:inline-block; vertical-align:middle; background-color:" + player.GetCurrentPropertyInBoard().owner.GetColor() + ";'></span> " + player.GetCurrentPropertyInBoard().owner.GetName();
        }
        infoPlayer += "</div>";
        $('.info-player').html(infoPlayer);
        this.DrawRendererButtons(reRender, player);
    };
    Board.prototype.DrawTurnSpecifications = function () {
        var renderData = "<div class='turno'></div><div class='nombreJugador'></div><div class='card-container'></div><div class='info-player'></div><div class='rendered-buttons'></div>";
        $("#" + this._SPECIFICATIONS_TURN_DETAIL_ID).html(renderData);
        $('.card-container').hide(0);
    };
    Board.prototype.DrawPlayers = function () {
        var countRestPlayers = this._MAX_PLAYERS;
        var renderedPlayers = "";
        for (var i in this._players) {
            renderedPlayers += "<div id='player" + i + "' style=' width:" + this._PLAYER_SIZE_PIECE_PX + "px; height:" + this._PLAYER_SIZE_PIECE_PX + "px; position: absolute; left: " + ((this._players[i].GetCurrentPropertyInBoard().posx + this._propertyNormalWidth / 2) - (countRestPlayers * this._PLAYER_SIZE_PIECE_PX)) + "px; bottom: " + (this._players[i].GetCurrentPropertyInBoard().posy + this._propertyNormalHeight / 2) + "px; border-radius: 50%; transition: all .5s; display:block; background-color: " + this._players[i].GetColor() + ";'>&nbsp;</div>";
            countRestPlayers--;
        }
        $("#" + this._BOARD_ID).append(renderedPlayers);
    };
    Board.prototype.PlayDice = function () {
        var result = Math.floor(Math.random() * this._NUM_MAX_DICES) + 1;
        $('#dice').attr('src', "img/dice-0" + result + ".png");
        return result;
    };
    Board.prototype.RedrawAll = function () {
        this.DrawBoard();
        this.DrawDice();
        this.DrawPlayers();
    };
    return Board;
}());
var game = new Board($('#board').outerWidth(), $('#board').outerHeight());
//# sourceMappingURL=main.js.map